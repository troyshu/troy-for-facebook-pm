$(function(){
    // nav toggle on mobile
    $('.nav-toggle').on('click', function() {
        $('.nav-menu').toggleClass('is-active');
    })

    // init isotope
    var $grid = $('.grid').isotope({
      // options
      itemSelector: '.grid-item',
      layoutMode: 'masonry'
    });
    var stampElems = $('.stamp');
    $grid.isotope('stamp', stampElems );

    // filter event listener
    $('.filter').on('click', function(e) {
        let filterButton = $(e.target);
        let filterClass = "." + filterButton.text().toLowerCase().replace(" ", "-");
        $grid.isotope({ filter: filterClass });
        $('.filter').removeClass('active');
        filterButton.addClass('active');
    })
    $('#show-all').on('click', function() {
        $('.filter').removeClass('active');
      $grid.isotope({ filter: '*' });  
    });

    // listeners for modal
    // modal close
    $('.close-button, .modal-background').on('click', function(e) {
        $('.modal').find('.modal-card-body .content').html(""); // clear modal body
        $('.modal').find('.modal-card-body .content').animate({ scrollTop: 0 }, 0); // scroll to top of content
        $(e.target).parents('.modal').removeClass('is-active');
    });
    // modal open
    $('.tile:not(.stamp-static) article').on('click', function(e) {
        let modalDataId = '#' + $(e.target).parents('.tile.is-parent').data('modal');
        //console.log(modalDataId);
        let currentModalBody = $('#modal-data').find(modalDataId);
        //console.log(currentModalBody);
        $('.modal').find('.modal-card-body .content').html(currentModalBody.html()); // set modal body
        $('.modal').addClass('is-active');
    });


});